#!/usr/bin python3

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
x = np.linspace(0,15,30)
y = np.sin(x) + 0.1*np.random.randn(len(x))

x2 = np.linspace(0, 15, 100)
y2 = np.sin(x2)

plt.figure(figsize=(8,3))
plt.plot(x,y, 'o', color='purple', label='Data', zorder=100) # zorder to let the data on top
plt.plot(x2,y2, color='olive', label='Fit')
plt.legend(loc='upper right', ncol=2) # if not ncol, line after line style it is

plt.ylim(top=2) # to add more space for the legend

plt.xlabel("Time t [s]")
plt.ylabel("Voltage V(t) [V]")
#plt.title("Voltage vs times")

plt.savefig("exp_vs_theory.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{exp_vs_theory.pgf}

#plt.savefig("exp_vs_theory.eps", format='eps')
#plt.savefig("exp_vs_theory.png", dpi=200) # this is a high-res dpi

plt.show()
