#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
res = np.random.randn(1000)*0.2 + 0.4
res2 = np.random.randn(1000)*0.2 + 0.4

textstr = '\n'.join((
    r'$\sigma_1=%.3f$' % (np.std(res)),
    r'$\sigma_2=%.3f$' % (np.std(res2)))) # sigma calcul to print it on the graph

fig, ax = plt.subplots(1, 1, figsize=(8,3))
ax.hist(res, bins=30, density=True, histtype='step', label="data 1") # density to make a density-histogramm (area = 1)
ax.hist(res2, bins=30, density=True, histtype='step', label="data 2")

ax.set_xlabel("$\hat{E}-E$")
ax.set_ylabel("Frequency")
#ax.set_title("title")
ax.legend(loc='upper right', fancybox=False, edgecolor='black')

# text to print: transform arg to absolute coordinates
ax.text(0.05, 0.75, textstr, transform=ax.transAxes,
       bbox=dict(facecolor='white', edgecolor='black'))

plt.savefig("double_hist-comparaison.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{double_hist-comparaison.pgf}

#plt.savefig("double_hist-comparaison.eps", format='eps')
#plt.savefig("double_hist-comparaison.png", dpi=200) # this is a high-res dpi

plt.show()

