#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
res = np.random.randn(1000)*0.2 + 0.4

plt.figure(figsize=(8,3))
plt.hist(res, bins=30, density=True) # density to make a density-histogramm (area = 1)

plt.xlabel("$\hat{E}-E$")
plt.ylabel("Frequency")
#plot.title("title")

plt.savefig("simple_hist.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{simple_hist.pgf}

#plt.savefig("simple_hist.eps", format='eps')
#plt.savefig("simple_hist.png", dpi=200) # this is a high-res dpi

plt.show()

