#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form_3D

#### Plotting
_ = np.linspace(-1, 1, 100)
x, y = np.meshgrid(_,_)
z = x**2+x*y

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
ax.plot_surface(x,y,z, cmap='coolwarm', linewidth=0, antialiased=False)
# see https://matplotlib.org/stable/tutorials/colors/colormaps.html#sphx-glr-tutorials-colors-colormaps-py for colors
ax.view_init(elev=10, azim=50) # view
ax.set_xlabel("Horizontal Position [m]")
ax.set_ylabel("Vertical Position [m]")
ax.set_zlabel("Temperature [$^\circ C$]")
#plt.title("title")

plt.savefig("simple_3D.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{simple_3D.pgf}

#plt.savefig("simple_3D.eps", format='eps')
#plt.savefig("simple_3D.png", dpi=200) # this is a high-res dpi

plt.show()

