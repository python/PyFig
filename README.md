# Plots with Python
To plot pretty science-stuff in an more-or-less academic way, as in lab-report, thesis, paper, etc.

Note that you'll need the `form.py` or `form_3D.py` to compile those scripts.

**Requirements (PyPI)**

``` bash
pip3 install --user numpy matplotlib scienceplots
```


**Export**

By default, those scripts are made to export the plots to a TikZ format (actually, PGFPlots) to integrate them easily into a LaTeX document.

To do so, you will need a `\usepackage{pgfplots}` in your tex-preambule, and then:

``` latex
\begin{figure}[htpb]
	\centering
	\input{your_export.pgf}
	\caption{\label{fig:my_fig}My caption.}
\end{figure}
```

For animations, you can choose to save frames by frames the figure in .png, and with a `\usepackage{animate,graphicx}` you can integrate this in a LaTeX-PDF (works with Adobe and Okular):

``` latex
\begin{figure}[htpb]
	\centering
	\animategraphics[autoplay,loop,width=.6\linewidth]{12}{your_export-}{0}{<NB_FRAMES-1>}
	\caption{\label{fig:my_fig}My caption.}
\end{figure}
```

**Credits**

Thanks to Luke from [Mr. P Solver](https://www.youtube.com/channel/UCKaYxkHrmsQePZFpzF9b7sQ) and his [repo of tutorials](https://github.com/lukepolson/youtube_channel/tree/main/Python%20Tutorial%20Series). I also give a very strong thanks to Chritian Hill and his book *Learning Scientific Programming with Python [2nd ed.] (2020)*.
