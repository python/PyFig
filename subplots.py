#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
x = np.linspace(0,15,30)
y = np.sin(x) + 0.1*np.random.randn(len(x))
res = np.random.randn(1000)*0.2 + 0.4
res2 = np.random.randn(1000)*0.2 + 0.4

fig, axes = plt.subplots(1, 2, figsize=(12,4))
ax = axes[0] # for more rows: axes[i][j]
ax.plot(x, y, 'o--', color='r', lw=0.4, ms=3)
ax.set_xlabel("Time t [s]")
ax.set_ylabel("Voltage V [V]")
#ax.set_title("title")

ax = axes[1]
ax.hist(res, bins=30, density=True, histtype='step', label='C1')
ax.hist(res2, bins=30, density=True, histtype='step', label='C2')
ax.set_xlabel("$\hat{E}-E$")
ax.legend()
#ax.set_title("title")

fig.tight_layout()
fig.suptitle('Title of All Plots', y=1.01, fontsize=14, ha='center')
#fig.text(0.5, -0.04, '$\Delta E$ [Joules]', ha='center', size=20) # commune abscisse, ha arg to center it

plt.savefig("subplots.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{subplots.pgf}

#plt.savefig("subplots.eps", format='eps')
#plt.savefig("subplots.png", dpi=200) # this is a high-res dpi

plt.show()

