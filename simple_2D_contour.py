#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

import form

#### Plotting
_ = np.linspace(-1, 1, 100)
x, y = np.meshgrid(_,_)
z = x**2+x*y

cs = plt.contour(x,y,z, levels=20)
plt.clabel(cs, fontsize=8) #, label="Temperature [$^\circ C$]")
#plt.colorbar(label='Temperature [$^\circ C$]')
plt.xlabel("Horizontal Position [m]")
plt.ylabel("Vertical Position [m]")
#plt.title("title")

plt.savefig("simple_2D_contour.pgf", format='pgf') 
    # then \renewcommand\ssfamily{} \input{simple_2D_contour.pgf}

#plt.savefig("simple_2D_contour.eps", format='eps')
#plt.savefig("simple_2D_contour.png", dpi=200) # this is a high-res dpi

plt.show()

