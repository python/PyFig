#!/usr/bin python

import numpy as np
import matplotlib.pyplot as plt

from matplotlib import animation
from matplotlib.animation import PillowWriter

import form_3D

#### Plotting
_ = np.linspace(-1, 1, 100)
x, y = np.meshgrid(_,_)
z = x**2+x*y

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
ax.plot_surface(x,y,z, cmap='coolwarm', linewidth=0, antialiased=False)
ax.view_init(elev=10, azim=0)

def animate(i):
    ax.view_init(elev=10, azim=3*i)
    #plt.savefig('animate_3D-{}.png'.format(i))
        #then with animate: \animategraphics[autoplay,loop,width=.6\linewidth]{12}{animate_2D-}{0}{239}

ani = animation.FuncAnimation(fig, animate, frames=120, interval=50)
ani.save('animate_3D.gif',writer='pillow',fps=50,dpi=100)
    # see https://holypython.com/how-to-save-matplotlib-animations-the-ultimate-guide/ for more vid formats

plt.show()

